export default {
    message: {
        hello: 'Привет мир'

    },
    labels:{
        username:'Логин',
        password: 'Пароль',
        signIn:'Войти',
        signInTitle:'Вход',
        firstName:'Имя',
        lastName: 'Фамилия',
        email: 'Email',
        registerTitle: 'Регистрация',
        registerButton:'Зарегистрироваться'
    }
}