export default {
    message: {
        hello: 'Hello world'

    },
    labels:{
        username:'Username',
        password: 'Password',
        signIn:'Sign In',
        signInTitle:'Sign In',
        firstName:'First Name',
        lastName: 'Last Name',
        email: 'Email',
        registerTitle: 'Register',
        registerButton:'Register'
    }
}