import VueI18n from 'vue-i18n';
import Vue from 'vue';
import ru from './ru';
import en from './en';
Vue.use(VueI18n)
const messages = {
    en, ru
}


export default new VueI18n({
    locale: 'en', // set locale
    messages, // set locale messages
})


