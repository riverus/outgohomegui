import RecordsTable from "./components/RecordsTable";
import Statistic from "./components/Statistic";
import Login from './components/auth/Login';


export const routes = [
    {
        path: "/",
        component: RecordsTable,
        meta: {
            requiresAuth: true
        }
    },
    {
        path: "/records",
        component: RecordsTable,
        meta: {
            requiresAuth: true
        }

    },
    {
        path: "/",
        component: RecordsTable,
        name:'home',
        meta: {
            requiresAuth: true
        }
    },
    {
        path: "/statistic",
        component: Statistic
    },
    {
        path:"/login",
        component:Login,
        name:"login"
    }
];

