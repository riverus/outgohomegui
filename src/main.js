import Vue from 'vue'
import App from './App.vue'
import "bootstrap/dist/css/bootstrap.min.css"
import 'vue-select/dist/vue-select.css';
import {router} from "./router";
import store from './store/store';
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import '@mdi/font/css/materialdesignicons.css'

import i18n from './i18n/i18n'

import ApiService from './store/service/api.service';
import {CHECK_AUTH} from './store/actions.type';
Vue.use(Vuetify);
ApiService.init();

// Ensure we checked auth before each page load.
router.beforeEach((to, from, next) =>
  Promise.all([store.dispatch(CHECK_AUTH)]).then(next)
);


new Vue({
    router,
    store,
  render: h => h(App),
    i18n
}).$mount('#app')




