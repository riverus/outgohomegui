import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";
import JwtService from "./jwt.service";
import { API_URL } from "./config";

const ApiService = {
  init() {
    Vue.use(VueAxios, axios);
    Vue.axios.defaults.baseURL = API_URL;
  },

  setHeader() {
    Vue.axios.defaults.headers.common[
      "Authorization"
    ] = `Bearer_${JwtService.getToken()}`;
  },

  query(resource, params) {
    let stringParams="";

    if(params!==undefined){
      let paramsArray=Object.keys(params);
      stringParams="?";
      for (let i=0; i<paramsArray.length; i++){
        stringParams=stringParams+paramsArray[i]+"="+params[paramsArray[i]]+"&";
      }
    }
    return Vue.axios.get(resource + stringParams).catch(error => {

      throw new Error(`[RWV] ApiService ${error}`);
    });
  },

  get(resource, slug = "") {
    return Vue.axios.get(`${resource}/${slug}`).catch(error => {
      throw new Error(`[RWV] ApiService ${error}`);
    });
  },

  post(resource, params) {
    return Vue.axios.post(`${resource}`, params);
  },

  update(resource, slug, params) {
    return Vue.axios.put(`${resource}/${slug}`, params);
  },

  put(resource, params) {
    return Vue.axios.put(`${resource}`, params);
  },

  delete(resource) {
    return Vue.axios.delete(resource).catch(error => {
      throw new Error(`[RWV] ApiService ${error}`);
    });
  }
};

export default ApiService;

export const RecordsService = {
  query(params) {
    return ApiService.query("records",
      params
    );
  },  
  create(params) {
    return ApiService.post("records", params);
  },
  update(slug, params) {
    return ApiService.update("records", slug, params);
  },
  destroy(slug) {
    return ApiService.delete(`records/${slug}`);
  }
};

export const CategoryService = {
  query(type, params) {
    return ApiService.query("categories", {
      params: params
    });
  },
  create(params) {
    return ApiService.post("categories", params);
  },
  update(slug, params) {
    return ApiService.update("categories", slug,  params);
  },
  destroy(slug) {
    return ApiService.delete(`categories/${slug}`);
  }
};

export const StatisticService = {
  query(type, params) {
    return ApiService.query("statistic",
     params
    );
  }
};



