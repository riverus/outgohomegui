import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

import recordsModule from './modules/records.module';
import categoryModule from './modules/categories.module';
import auth from './modules/auth.module';
import statisticModule from './modules/statistic.module'

Vue.use(Vuex, axios);


export default new Vuex.Store({
    modules: {
        recordsModule,
        categoryModule,
        auth,
        statisticModule
    }
})