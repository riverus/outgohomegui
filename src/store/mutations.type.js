export const SET_RECORDS = "setRecords";
export const SET_CATEGORIES = "setCategories";
export const SET_STATISTIC = "setStatistic";
export const SET_AUTH = "setAuth";
export const SET_ERROR = "setError";
export const PURGE_AUTH = "purgeAuth";