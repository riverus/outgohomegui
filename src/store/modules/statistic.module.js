import {
    StatisticService
} from "../service/api.service";
import {
    STATISTIC_LOAD
} from "../actions.type";
import {
    SET_STATISTIC
} from "../mutations.type";

const initialState = {
    statistic: []
};

export const state = { ...initialState };

export const actions = {
    async [STATISTIC_LOAD](context) {
        const { data } = await StatisticService.query() ;
        context.commit(SET_STATISTIC, data);
        return data;
    }
};

/* eslint no-param-reassign: ["error", { "props": false }] */
export const mutations = {
    [SET_STATISTIC](state, statistic) {
        state.statistic = statistic;
    }
};

const getters = {
    statistic(state) {
        return state.statistic;
    }
};

export default {
    state,
    actions,
    mutations,
    getters
};
