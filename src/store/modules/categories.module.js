import {
    CategoryService
} from "../service/api.service";
import {
    CATEGORIES_LOAD,
    CATEGORY_CREATE,
    CATEGORY_DELETE,
    CATEGORY_UPDATE, RECORDS_LOAD
} from "../actions.type";
import {
    SET_CATEGORIES
} from "../mutations.type";

const initialState = {
    categories: []
};

export const state = { ...initialState };

export const actions = {
    async [CATEGORIES_LOAD](context) {
        const { data } = await CategoryService.query() ;
        context.commit(SET_CATEGORIES, data);
        return data;
    },
    async [CATEGORY_CREATE](context, payload) {
        await CategoryService.create(payload);
        context.dispatch(CATEGORIES_LOAD);
    },
    async [CATEGORY_DELETE](context, payload) {
        await CategoryService.destroy(payload);
        context.dispatch(CATEGORIES_LOAD);
        context.dispatch(RECORDS_LOAD);
    },
    async [CATEGORY_UPDATE](context, payload) {
        await CategoryService.update(payload.category.id, payload.category);
        context.dispatch(CATEGORIES_LOAD);
    }
};

/* eslint no-param-reassign: ["error", { "props": false }] */
export const mutations = {
    [SET_CATEGORIES](state, categories) {
        state.categories = categories;
    }
};

const getters = {
    categories(state) {
        return state.categories;
    }
};

export default {
    state,
    actions,
    mutations,
    getters
};
