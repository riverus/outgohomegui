import {
    RecordsService
} from "../service/api.service";
import {
    RECORDS_LOAD,
    RECORD_CREATE,
    RECORD_UPDATE,
    RECORD_DELETE
} from "../actions.type";
import {
    SET_RECORDS
} from "../mutations.type";

const initialState = {
    records: []
};

export const state = {...initialState};

export const actions = {
    async [RECORDS_LOAD](context, params) {
        const {data} = await RecordsService.query(params);
        context.commit(SET_RECORDS, data);
        return data;
    },
    async [RECORD_CREATE](context, payload) {
        await RecordsService.create(payload);
        context.dispatch(RECORDS_LOAD);
    },
    async [RECORD_DELETE](context, payload) {
        await RecordsService.destroy(payload);
        context.dispatch(RECORDS_LOAD);
    },
    async [RECORD_UPDATE](context, payload) {
        await RecordsService.update(payload.record.id, payload.record);
        context.dispatch(RECORDS_LOAD);
    }
};

/* eslint no-param-reassign: ["error", { "props": false }] */
export const mutations = {
    [SET_RECORDS](state, records) {
        state.records = records;
    }
};

const getters = {
    records(state) {
        return state.records;
    }
};

export default {
    state,
    actions,
    mutations,
    getters
};
