import JwtService from "./../service/jwt.service";
import ApiService from './../service/api.service';
import {
  LOGIN,
  LOGOUT,
  CHECK_AUTH,
  REGISTER
} from "./../actions.type";
import { SET_AUTH, PURGE_AUTH, SET_ERROR } from "./../mutations.type";

const state = {
  errors: null,
  user: {},
  isAuthenticated: !!JwtService.getToken()
};

const getters = {
  currentUser(state) {
    return state.user;
  },
  isAuthenticated(state) {
    return state.isAuthenticated;
  }
};

const actions = {
  [LOGIN](context, credentials) {
    return new Promise(resolve => {
      ApiService.post("/auth/login", { username: credentials.username,
      password: credentials.password })
        .then(({ data }) => {
          context.commit(SET_AUTH, data.user);
          resolve(data);
        })
        .catch((error) => {
          context.commit(SET_ERROR, error);
          context.commit(PURGE_AUTH);
        });
    });
  },
  [LOGOUT](context) {
    context.commit(PURGE_AUTH);
  },
  [CHECK_AUTH](context) {
    if (JwtService.getToken()!==null) {
      ApiService.setHeader();
      ApiService.get("user")
        .then(({ data }) => {
          context.commit(SET_AUTH, data.user);
        })
        .catch(({ response }) => {
          context.commit(SET_ERROR, response.data.errors);
        });
    } else {
      context.commit(PURGE_AUTH);
    }
  },
  
  [REGISTER](context, credentials) {
    return new Promise((resolve, reject) => {
      ApiService.post("/auth/register", credentials)
        .then((response) => {
          // context.commit(SET_AUTH, response.data.user);
          resolve(response) ;
        })
        .catch((response ) => {
          context.commit(SET_ERROR, response.data.errors);
          reject(response);
        });
    });
  }
};

const mutations = {
  [SET_ERROR](state, error) {
    state.errors = error;
  },
  [SET_AUTH](state, user) {
    state.isAuthenticated = true;
    state.user = user;  
    state.errors = {};
    JwtService.saveToken(state.user.token);
  },
  [PURGE_AUTH](state) {
    state.isAuthenticated = false;
    state.user = {};
    state.errors = {};
    JwtService.destroyToken();
  }
};

export default {
  state,
  actions,
  mutations,
  getters
};
